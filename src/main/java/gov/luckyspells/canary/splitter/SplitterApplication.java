package gov.luckyspells.canary.splitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.app.splitter.processor.SplitterProcessorConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SplitterProcessorConfiguration.class)
public class SplitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SplitterApplication.class, args);
	}
}
